$("a").click(function(){
    let elementClick = $(this).attr("href");
    let destination = $(elementClick).offset().top;
    $('html, body').animate({ scrollTop: destination }, 2000);
});
$('body').append('<button id="scrollTop">^</button>')
$('#scrollTop').css({visibility:'hidden',width:30,height:25,position:'fixed',right:20,top:620,
background: 'green',border:0,cursor:'pointer'})

$(window).scroll(function(){
	$('#scrollTop').css('visibility','visible')
	if($(window).scrollTop() == 0){
		$('#scrollTop').css('visibility','hidden')
	}
})
$('#scrollTop').click(function(){
	$('html, body').animate({ scrollTop: 0 }, 2000)
})

$('#toggleButton').click(function(){
	$('#scroll-7').slideToggle('slow')
})